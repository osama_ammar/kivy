# -*- coding: utf-8 -*-
''' this is a broject to make a radioisotope timer to schedule the preparation of a radioisotope and injection of them into patients'''

# This code is licensed under the GNU License version 3 (GPLv3)
# kivy_projects  Copyright (C) 2020  osama sayed hanafy  mohamed 
# for more information about the project --> ossssamar@gmail.com

from __future__ import division
import datetime
import kivy
kivy.require('1.11.1')
from kivy.app import App
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty, NumericProperty, StringProperty
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.uix.spinner import Spinner
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.image import Image
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition
from kivy.uix.screenmanager import FadeTransition
#from kivy.garden import DatetimePicker
from kivy.base import runTouchApp
import time
from kivy.input.shape import ShapeRect
#from plyer import vibrator
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.tabbedpanel import TabbedPanelHeader
from kivy.garden.circulardatetimepicker import CircularTimePicker
Window.size = (400, 750)





class MyGrid(Screen):
    def __init__(self, **kwargs):
        super(MyGrid, self).__init__(**kwargs)
#        self.cols = 1
        self.layout = BoxLayout(orientation='vertical')
			
        self.comment_box = TextInput(multiline=True,text='Enter some comments..(ex) this patient is diabetic..',size_hint_y=None,height=50,size_hint_x=None,width=300,pos_hint={'x':.1, 'y':.0})        
			
        self.Required_Dose = TextInput(multiline=False,text='0',size_hint=(.7, 1))	
        self.Injection_Time = TextInput(multiline=False,text='0')
        self.prepare = Label(text='0.00')
        self.spinnerObject = Spinner(text ="Isotope", 
             values =("Tc 99m", "F 18","I 131", "I 123", "Ga 68", "Ga 67","Y 90","O 15","N 13"), 
             background_color =(0.24, 0.243, 0.96, 2),size_hint_y=None,height=50,size_hint_x=None,width=200,pos_hint={'x':.25, 'y':.4})
        self.units = Spinner(text ="mci", 
             values =( "mci","ci", "MBq", "Bq", "GBq"), 
             background_color =(0.6, 0.93, 0.96, 2))        
#        self.units1 = Spinner(text ="mci", 
#             values =("mci","ci", "MBq", "Bq", "GBq"), 
#             background_color =(0.6, 0.93, 0.96, 2))  
        self.units1=(Label(text='mci'))#,size_hint_y=None,height=90,size_hint_x=None,width=110,pos_hint={'x':.8, 'y':.0})) 
       							
        self.inside = GridLayout(spacing=[1.1])
        self.inside.cols = 3  
        
        self.inside.add_widget(Label(text="Required Dose : ",size_hint=(.3, 1)))
        self.inside.add_widget(self.Required_Dose)
        self.inside.add_widget(self.units)   
        
        self.inside.add_widget(Label(text="Prepare "))
        self.inside.add_widget(self.prepare)
        self.inside.add_widget(self.units1)           
        

        self.nuclide = BoxLayout(orientation='vertical')        
        self.nuclide.add_widget(self.spinnerObject)		        




        self.c = CircularTimePicker()
        self.c.bind()
        self.calpopup = Popup(title='Time',content=self.c,size_hint=(None, None), size=(150, 200))
#        self.calpopup.bind(on_dismiss=self.time_catch) 
								
        self.time_in = Label(text='0',size_hint_y=None,height=30,size_hint_x=None,width=50,pos_hint={'x':.5, 'y':0.0})
								
        self.time_catcher = Button(text="select time", background_color=[1,2,3,3],font_size=15,size_hint_y=None,height=30,size_hint_x=None,width=90,pos_hint={'x':.7, 'y':0.0})        
        self.time_catcher.bind(on_press=self.time_catch)
								
        self.clockw = FloatLayout(size=(300, 100))#orientation='horizontal')
								
        self.clockw.add_widget(Label(text="Injection Time ",size_hint_y=None,height=30,size_hint_x=None,width=50,pos_hint={'x':.1, 'y':0.0})) 														
#        self.clockw.add_widget(Label(text=" Hour",size_hint_y=None,height=70,size_hint_x=None,width=70))								
        self.clockw.add_widget(self.time_in)
        self.clockw.add_widget(self.time_catcher)



        self.wimg = Image(source='science.png')
        self.spect_img = Image(source='gas-mask.png')								

        self.submit = Button(text="Calculate", background_color=[1,2,3,4],font_size=25,size_hint_y=None,height=50,size_hint_x=None,width=200,pos_hint={'x':.25, 'y':.3})
        self.submit.bind(on_press=self.pressed)
        self.tab_mover = Button(text="+", background_color=[1,1,1,0],font_size=60,size_hint_y=None,height=10,size_hint_x=None,width=10,pos_hint={'x':.85, 'y':.0})        
        self.tab_mover.bind(on_press=self.tab_moving)        	        
        self.f_layout = FloatLayout(size=(300, 300))
        self.f_layout.add_widget(self.submit)	
        self.f_layout.add_widget(self.tab_mover)								
        self.popup = Popup(title='Test popup',content=Label(text='Hello world'),size_hint=(None, None), size=(150, 200))
        
        
#        self.tab_mover = Button(text="+", background_color=[6,1,3,1],font_size=50,size_hint_y=None,height=50,size_hint_x=None,width=200,pos_hint={'x':.1, 'y':.1})        
####################################################################
        self.vibrate_box= GridLayout(spacing=[2])
        self.vibrate_box.cols = 2  
        self.vib_label = Label(text='vibrate at injection time (coming soon..)', size_hint_x=None, width=300)
        self.vib_check =CheckBox() 
        self.vibrate_box.add_widget(self.vib_label)
        self.vibrate_box.add_widget(self.vib_check)								
########################################################################3								
        self.layout.add_widget(self.wimg)
        self.layout.add_widget(self.comment_box)									
        self.layout.add_widget(Label(text="Injection Timer ",color=[2,1,0,2],size_hint=(.5, .5),pos_hint={'x':.25, 'y':.3}))		
        self.layout.add_widget(self.clockw)						
        self.layout.add_widget(self.inside)	
        self.layout.add_widget(self.nuclide)							
        self.layout.add_widget(self.f_layout)
        self.layout.add_widget(self.vibrate_box)								
        
																																		
#        self.c = CircularTimePicker()
#        self.c.text='4,7'
#        self.c.bind()
					
						
#        self.pick=DatetimePicker()
#        self.add_widget(self.pick)																					
        time_now = (str(datetime.datetime.now().time().strftime("%H:%M:%S")))
        hr,minu,sec = time_now.split(":")
        clear_time=str(int(hr))+':'+str(int(minu))+':'+str(int(sec))
        
        self.updated_clock=Label(text=clear_time)      
        self.layout.add_widget(self.updated_clock)        
        self.clo=Clock.schedule_interval(self.update_time, 1)
        
        
        self.tb_panel= TabbedPanel(do_default_tab=False)
         
        self.th_btn_head = TabbedPanelHeader(text=' Patient 1')
        self.th_btn_head.content= self.layout        
        self.tb_panel.add_widget(self.th_btn_head)
								
        self.add_widget(self.spect_img)
        self.add_widget(self.tb_panel)        
        self.n=2

        		
    def update_time(self, *args):	
        self.time_now = (str(datetime.datetime.now().time().strftime("%H:%M:%S")))
        hr,minu,sec = self.time_now.split(":")
        clear_time=str(int(hr))+':'+str(int(minu))+':'+str(int(sec))
        self.updated_clock.text=(clear_time)																													
        self.time_in.text=str(self.c.time)


#############################################################3
        inj_time = (str(self.c.time))
        hr_in,min_in,sec_in = inj_time.split(":")
        inj_time=[float(hr_in)*60,float(min_in),float(sec_in)]
        inj_time_min= inj_time[0]+inj_time[1] 	
								
        self.hr,self.minu,self.sec = self.time_now.split(":")
        self.time_now=[float(self.hr)*60,float(self.minu)]
        self.vib_time= self.time_now[0]+self.time_now[1]

        if self.vib_check.active==True:  									
                if inj_time_min== self.vib_time  :print ('time come')														
#                vibrator.vibrate(5)							
								
									
 
    def time_catch(self, *args):
         self.calpopup.open()								
#         self.time_in.text=str(self.c.time)
#         print (self.c.time)													
#        
         
    def tab_moving(self,*args):
                num_of_tabs=len(self.tb_panel.tab_list)
                print (num_of_tabs)
                self.th_text_head = TabbedPanelHeader(text=' Patient %d'%(num_of_tabs+1))													
                self.th_text_head.content= pt_list[num_of_tabs+1]   
                self.tb_panel.add_widget(self.th_text_head)
#                self.n+=1
#                print ('called %d'%self.n)
#                print (num_of_tabs)

                
    def on_touch_up(self, touch):
#        if self.collide_point(*touch.pos):
         if touch.dpos[1]>5: 
#               print (touch.dpos[1])
                self.manager.transition = SlideTransition(direction="up")
                self.manager.current = self.manager.next()   
         
    def pressed(self, instance):
#        self.popup.open()

        print (self.c.time.hour)			
					
        time_now = (str(datetime.datetime.now().time()))
        hr,minu,sec = time_now.split(":")
        time_now=[float(hr)*60,float(minu),float(sec)]
        time_min= time_now[0]+time_now[1] 
         	  				
					
        Required_Dose = float(self.Required_Dose.text)
								
        inj_time = (str(self.c.time))
        hr_in,min_in,sec_in = inj_time.split(":")
        inj_time=[float(hr_in)*60,float(min_in),float(sec_in)]
        inj_time_min= inj_time[0]+inj_time[1] 								

								
        #activity_pre = float(self.prepare.text)
        iso_dict ={"Tc 99m":.001918, "F 18":.009, "I 131":.0001, "I 123":.005, "Ga 68":.0106, "Ga 67":.0001,"Y 90":0.002,"O 15":0.3462,"N 13":0.093}
        for i in iso_dict.keys():				
            if  self.spinnerObject.text==i: decay_const=float(iso_dict[i])	
            
        activity_pre = Required_Dose/( 2.71828**( -decay_const*(inj_time_min-time_min) )) 
           
        units_dict={"mci":1,"ci":1, "MBq":1, "Bq":1, "GBq":1}	
        for i in units_dict.keys():				
            if  self.units.text==i: 
                activity_pre*=units_dict[i] 
                self.units1.text=i
        
        print ("prepare now ",activity_pre,"mci")
#        print	 (self.cl.minutes)						
        												
#        print("Name:", Required_Dose, "Last Name:", inj_time, "prepare:", prepare,"isotope :",isotope)
#        self.Required_Dose.text = ""
#        self.Injection_Time.text = ""
        self.prepare.text = "%.2f"%activity_pre


class Decay(Screen):
    def __init__(self, **kwargs):
        super( Decay, self).__init__(**kwargs)

        self.layout = BoxLayout(orientation='vertical')  

        self.wimg = Image(source='gas-mask.png')

        self.comment_box = TextInput(multiline=True,text='Enter some comments........',size_hint_y=None,height=50,size_hint_x=None,width=300,pos_hint={'x':.1, 'y':.0})        
        
        self.dose_box = GridLayout(spacing=[20])
        self.dose_box.cols =4         
 #       self.dose_box = BoxLayout(orientation='horizontal') 
        self.start = Button(text="Ref. Time Now", background_color=[1,2,3,3],font_size=15,size_hint_y=None,height=30,size_hint_x=None,width=120)        
        self.start.bind(on_press=self.c_time)        
        self.dose_box.add_widget(Label(text="Dose",size_hint_y=None,height=50,size_hint_x=None,width=70))
        self.Required_Dose = TextInput(multiline=False,text='0',size_hint_y=None,height=50,size_hint_x=None,width=70)        
        self.units = Spinner(text ="mci", 
             values =( "mci","ci", "MBq", "Bq", "GBq"), 
             background_color =(0.6, 0.93, 0.96, 2),size_hint_y=None,height=50,size_hint_x=None,width=70)  
        self.dose_box.add_widget(self.Required_Dose)  
        self.dose_box.add_widget(self.units) 
        self.dose_box.add_widget(self.start) 

        
        self.c = CircularTimePicker()
        self.c_time_now = (str(datetime.datetime.now().time()))
        self.c_hr,self.c_minu,self.c_sec = self.c_time_now.split(":")
        self.c_time_now_list=[int(self.c_hr),int(self.c_minu)]								
        self.c.time_list=self.c_time_now_list								
        self.c.bind()
        self.calpopup = Popup(title='Time',content=self.c,size_hint=(None, None), size=(150, 200))
        self.calpopup.bind(on_dismiss=self.time_catch2) 
								
        self.time_in = Label(text='00:00:00',size_hint_y=None,height=30,size_hint_x=None,width=50,pos_hint={'x':.5, 'y':0.0})
								
        self.time_catcher = Button(text="select time", background_color=[1,2,3,3],font_size=15,size_hint_y=None,height=30,size_hint_x=None,width=90,pos_hint={'x':.7, 'y':0.0})        
        self.time_catcher.bind(on_press=self.time_catch)
								
        self.clockw = FloatLayout(size=(300, 100))#orientation='horizontal')
								
        self.clockw.add_widget(Label(text="Reference Time ",size_hint_y=None,height=30,size_hint_x=None,width=50,pos_hint={'x':.1, 'y':0.0})) 														
#        self.clockw.add_widget(Label(text=" Hour",size_hint_y=None,height=70,size_hint_x=None,width=70))								
        self.clockw.add_widget(self.time_in)
        self.clockw.add_widget(self.time_catcher)

       
#        self.isotope_box = BoxLayout(orientation='vertical')
        self.spinnerObject = Spinner(text ="Tc 99m", 
             values =("Tc 99m", "F 18","I 131", "I 123", "Ga 68", "Ga 67","Y 90","O 15","N 13"), 
             background_color =(0.24, 0.743, 0.96, 2),size_hint_y=None,height=50,size_hint_x=None,width=70,pos_hint={'x':.4, 'y':.0})                
#        self.isotope_box.add_widget(self.spinnerObject)
  
        
        self.current_dose_box = BoxLayout(orientation='horizontal')        
        self.current_dose_box.add_widget(Label(text="Dose Now: ",size_hint_y=None,height=70,size_hint_x=None,width=120,pos_hint={'center_x':.5, 'y':.0}))
        self.labe='.......'							
        self.current_dose_label=(Label(text=self.labe,size_hint_y=None,height=90,size_hint_x=None,width=110,pos_hint={'x':.8, 'y':.0})) 

        self.units2 = Spinner(text ="mci", 
             values =( "mci","ci", "MBq", "Bq", "GBq"), 
             background_color =(0.6, 0.93, 0.96, 2),size_hint_y=None,height=70,size_hint_x=None,width=100,pos_hint={'x':6, 'y':.0})  
        self.current_dose_box.add_widget(self.current_dose_label)  
#        self.current_dose_box.add_widget(self.units2) 

        self.submit = Button(text="set ref. now", background_color=[2,2,3,4],font_size=30,size_hint_y=None,height=50,size_hint_x=None,width=90,pos_hint={'x':.4, 'y':.0})
        self.submit.bind(on_press=self.c_time)
                
        Clock.schedule_interval(self.update_decay, 1)
								

        self.time_now = (str(datetime.datetime.now().time()))
        self.hr,self.minu,self.sec = self.time_now.split(":")
        self.time_now=[float(self.hr)*60,float(self.minu),float(self.sec)/60]
#        self.ref_time= self.time_now[0]+self.time_now[1] 	

								
        self.tab_mover = Button(text="+", background_color=[1,1,1,0],font_size=60,size_hint_y=None,height=10,size_hint_x=None,width=10,pos_hint={'x':.85, 'y':.5})        
        self.tab_mover.bind(on_press=self.tab_moving)
								
#        self.tab_delete = Button(text="-", background_color=[1,1,1,0],font_size=60,size_hint_y=None,height=10,size_hint_x=None,width=10,pos_hint={'x':.15, 'y':.5})        
#        self.tab_delete.bind(on_press=self.tab_deleting)


        self.f_layout = FloatLayout(size=(300, 300))
#        self.f_layout.add_widget(self.submit)	
        self.f_layout.add_widget(self.tab_mover) 
#        self.f_layout.add_widget(self.tab_delete)													
					
						
        self.tb_panel= TabbedPanel(do_default_tab=False)
        
        self.th_btn_head = TabbedPanelHeader(text=' Dose 1')
        self.th_btn_head.content= self.layout        
        self.tb_panel.add_widget(self.th_btn_head)

        self.layout.add_widget(self.wimg)
        self.layout.add_widget(self.comment_box)								
        self.layout.add_widget(self.dose_box)
        self.layout.add_widget(self.spinnerObject)								
        self.layout.add_widget(self.clockw)                
        self.layout.add_widget(self.current_dose_box)        
        self.layout.add_widget(self.f_layout)		
        
        self.add_widget(self.tb_panel)     											
#        self.add_widget(self.layout)
    #    self.Ref_time_iso=0								
        self.clo=Clock.schedule_interval(self.update_time, 1)						
        self.n=2 
    def c_time(self,*args):

        self.time_now = (str(datetime.datetime.now().time().strftime("%H:%M:%S")))
        hr,minu,sec = self.time_now.split(":")
        clear_time=str(int(hr))+':'+str(int(minu))+':'+str(int(sec))
        
        self.time_in.text=(clear_time)

    def update_time(self, *args):	
        pass																		
#         self.time_in.text=str(self.c.time)
									
#    def start_fun(self, *args):	
#        Clock.schedule_interval(self.update_decay, 1)								
        
    def update_decay (self,*args):  
            try:   
                ref_Dose = float(self.Required_Dose.text)
            except ValueError:   
                ref_Dose = float(0)
            
            time_now = (str(datetime.datetime.now().time()))
            hr,minu,sec = time_now.split(":")
            time_now=[float(hr)*60,float(minu),float(sec)/60]
            time_min= time_now[0]+time_now[1]+time_now[2] 

            ref_hr,ref_minu,ref_sec =self.time_in.text.split(":")
            time_ref=[float(ref_hr)*60,float(ref_minu),float(ref_sec)/60]
            time_ref= time_ref[0]+time_ref[1]+time_ref[2] 



            iso_dict ={"Tc 99m":.001918, "F 18":.009, "I 131":.0001, "I 123":.005, "Ga 68":.0106, "Ga 67":.0001,"Y 90":0.002,"O 15":0.3462,"N 13":0.093}
 #           decay_const=.0012
            for i in iso_dict.keys():				
                if  self.spinnerObject.text==i: decay_const=float(iso_dict[i])	
                
            activity_pre = ref_Dose * ( 2.71828**( -decay_const*(time_min-time_ref) ))
#            print (time_min-time_ref)
            self.Required_Dose.text										
            units_dict={"mci":1,"ci":1, "MBq":1, "Bq":1, "GBq":1}	
            for i in units_dict.keys():				
                if  self.units.text==i:
						
                  activity_pre*=units_dict[i] 
#                  self.units2.text=i
                  self.current_dose_label.text='%8.5f  %s'%(activity_pre,i)		

#                  self.Required_Dose.text=str(float(self.Required_Dose.text)*units_dict[i])																															
 #                 self.Required_Dose.text='%8.5f '%(self.Rd*units_dict[i])										
        

    def time_catch(self, *args):
         self.calpopup.open()
         inj_time = (str(self.c.time))
         hr_in,min_in,sec_in = inj_time.split(":")
         self.inj_time=[float(hr_in)*60,float(min_in),float(sec_in)]        
         self.Ref_time_iso= self.inj_time[0]+self.inj_time[1] 	
              									
                 									
									
         #self.ref_time= self.inj_time[0]+self.inj_time[1] 								

#         print (self.c.time)													
#        
    def time_catch2(self, *args): 
        inj_time = (str(self.c.time))
        hr_in,min_in,sec_in = inj_time.split(":")
        self.inj_time=[float(hr_in)*60,float(min_in),float(sec_in)]        
#        self.ref_time= self.inj_time[0]+self.inj_time[1] 	
        self.time_in.text=inj_time					
	       
    def on_touch_up(self, touch):
#        if self.collide_point(*touch.pos):
         if touch.dpos[1]>5: 
            print (touch.dpos[1])
            self.manager.transition = SlideTransition(direction="up")
            self.manager.current = self.manager.next() 
												
												
    def tab_moving(self,*args):

                self.th_text_head = TabbedPanelHeader(text=' Dose %d'%self.n)													
                self.th_text_head.content= dose_list[self.n]   
                self.tb_panel.add_widget(self.th_text_head)
                self.n+=1											
 
    def tab_deleting(self,*args):

 #               self.th_text_head = TabbedPanelHeader(text=' Dose %d'%self.n)													
#                self.th_text_head.content= dose_list[self.n] 
                print(self.tb_panel.current_tab)
                self.tb_panel.remove_widget(self.tb_panel.current_tab)
		#	tb_panel.content.children   #tb_panel.tab_list #tb_panel.current_tab	
                
                




d1=MyGrid();d2=MyGrid();d3=MyGrid();d4=MyGrid();d5=MyGrid()
d6=MyGrid();d7=MyGrid();d8=MyGrid();d9=MyGrid();d10=MyGrid();d11=MyGrid()
d12=MyGrid();d13=MyGrid();d14=MyGrid();d15=MyGrid();d16=MyGrid();d17=MyGrid();d18=MyGrid();d19=MyGrid();d20=MyGrid()

d_groub=[d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20]		
pt_list=[]
for i in d_groub:
    i.remove_widget(i.tab_mover)
    pt_list.append(i.layout)
#pt_list=[d1.layout,d2.layout,d3.layout,d4.layout,d5.layout,d6.layout,d7.layout,d8.layout,d9.layout,d10.layout,d11.layout,d12.layout,d13.layout,d14.layout,d15.layout,d16.layout,d17.layout,d18.layout,d19.layout,d20.layout]
#####################################################    
c1=Decay();c2=Decay();c3=Decay();c4=Decay();c5=Decay()
c6=Decay();c7=Decay();c8=Decay();c9=Decay();c10=Decay();c11=Decay()
c12=Decay();c13=Decay();c14=Decay();c15=Decay();c16=Decay();c17=Decay()		
dose_list=[c1.layout,c2.layout,c3.layout,c4.layout,c5.layout,c6.layout,c7.layout,c8.layout,c9.layout,c10.layout,c11.layout,c12.layout,c13.layout,c14.layout,c15.layout,c16.layout,c17.layout]


class RadioshApp(App):

    # 'build' is a method of App used in the framework it's
    # expected that the method returns an object of a Kivy widget

    def build(self):
        # Get an object of some widget that will be the core
        # of the application - in this case ScreenManager
        root = ScreenManager()


        # Add 4 CustomScreens with name 'Screen <order>`
#        for x in range(4):
#            root.add_widget(CustomScreen(name='Screen %d' % x))
												
        root.add_widget(MyGrid(name='Patients'))											
        root.add_widget(Decay(name='Doses'))
        # Return the object
        return root


#

				
				
#class MyApp(App):
#    def build(self):
#        return MyGrid()
							
if __name__ == "__main__":
    RadioshApp().run()	
#    MyApp().run()


























